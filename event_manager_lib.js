
/*************************************************************************************
 * create_slot_object
 *
 * setup role and slot selectors to enable selection of an available slot given a role
 * or an available role, given a slot.  
 *
 * Parameters:
 *
 *    availableSlots - array of slot object containing available slot information 
 *    tsId           - css selector of timeslot selector (a select object)
 *    rsId           - css selector of roleslot selector (a select object)
 *
 **************************************************************************************/

function create_slot_object(sid, rid, role_name, role_description, slot_start, slot_end)
{
    var object = new Object;
	
	object.sid = sid;
	object.rid = rid;
	object.role_name = role_name;
	object.role_description = role_description;
	object.slot_start = slot_start;
	object.slot_end = slot_end;
	return object;
}

function create_slot_objects(values)
{
   var aRecords = values.split("~");
   var aRecord;
   var aSlots = new Array;  
   var n;
   for (n=0;n<aRecords.length;n++){
     aRecord = aRecords[n].split("`");
	 aSlots[n] = create_slot_object(aRecord[0], aRecord[1], aRecord[2], aRecord[3], aRecord[4], aRecord[5]);
	 }
return aSlots;


}

/*************************************************************************************
 * setup_slot_role_selectors
 *
 * setup role and slot selectors to enable selection of an available slot given a role
 * or an available role, given a slot.  
 *
 * Parameters:
 *
 *    availableSlots - array of slot object containing available slot information 
 *    tsId           - css selector of timeslot selector (a select object)
 *    rsId           - css selector of roleslot selector (a select object)
 *
 **************************************************************************************/
 
function setup_slot_role_selectors(availableSlots, elementSlot, elementRole, sid, rid)
{
   var n;

   var objectSlot = $(elementSlot);
   var objectRole = $(elementRole);
   objectRole.val("");
   available_slots = availableSlots;
   $(elementSlot).bind("change", function () { populate_roles_for_slot(elementSlot, elementRole);});
   $(elementRole).bind("change", function () { populate_slots_for_role(elementRole, elementSlot);});

   if (rid) populate_roles_for_slot(elementSlot, elementRole, rid);
   if (sid) populate_slots_for_role(elementRole, elementSlot, sid);
   
}

function populate_roles_for_slot(elementSlot, elementRole, rid)
{
   var choices = "";
   var n;
   var objectSlot = $(elementSlot);
   var objectRole = $(elementRole);
   var domSlot = objectSlot.get(0);
   var domRole = objectRole.get(0);

   if (!rid) rid = domRole.currentValue;
   
// Save current value   
   domSlot.currentValue = objectSlot.val();
  
  
   for (n=0;n<available_slots.length;n++)
     if (available_slots[n].sid == objectSlot.val()){
	     if (choices.length > 0) choices+="~";
	     choices += available_slots[n].role_name+"`"+available_slots[n].rid;
		 }

	set_options(elementRole, choices, rid);	
		 
	return;
}


function populate_slots_for_role(elementRole, elementSlot)
{
   var choices = "";
   var n;
   var objectSlot = $(elementSlot);
   var objectRole = $(elementRole);

   var domRole = objectRole.get(0);
   var domSlot = objectSlot.get(0);

// Save current value   
   domRole.currentValue = objectRole.val();
   
   for (n=0;n<available_slots.length;n++)
     if (available_slots[n].rid == objectRole.val()){
	     if (choices.length > 0) choices+="~";
	     choices += available_slots[n].slot_start+"`"+available_slots[n].sid;
		 }
		
	set_options(elementSlot, choices, domSlot.currentValue);	
		 
	return;
}


function set_options(element, choices, currentValue)
{
  var html="";
  var aChoices = choices.split("~");
  var n;
  for (n=0;n<aChoices.length;n++){
    content = aChoices[n].split("`");
    html+="<option ";
	if (content[1] == currentValue) html += "selected ";
	html += "value='"+content[1]+"'>"+content[0]+"</option>";
	}
  $(element).empty();
  $(element).append(html);
}


function setup_show_elements_for_type(id)
{
   $(id).bind("click", function () { show_elements_for_type(id);});
   show_elements_for_type(id);
}

function show_elements_for_type(id)
{

    var i;
	var selected = -1;
	var values = $(id);
	
	for (i=0;i<values.length;i++) {
	   if (values[i].checked) selected = i;
	}
	
	if (selected == 1) { // Simple Registration (No roles or timeslots)
	    $('#tslots').hide();
	    $('#roles').hide();
	    $('#max_participants').show();
		  } else
	if (selected == 2) { // Participants register for a role
	    $('#tslots').hide();
	    $('#max_participants').hide();
	    $('#roles').show();
		  $('#roles legend a').html('Participant Roles');
		  $('#role-def-frame #but-addrow').val('Add Role');
		  $('#role-def-frame label').html('Enter the roles for which participants can register:');
		  jTable_upd_column_head(jTable_get(0), 1, 'Role');
		  } else
	if (selected == 3) { // Participants register for a timeslot
	    $('#tslots').show();
	    $('#max_participants').show();
	    $('#roles').hide();
		  } else
	if (selected == 4) { // Participants register for a timeslot and a role
	    $('#tslots').show();
	    $('#max_participants').hide();
	    $('#roles').show();
		  $('#roles legend a').html('Participant Roles');
		  $('#role-def-frame #but-addrow').val('Add Role');
		  $('#role-def-frame label').html('Enter the roles for which participants can register:');
		  jTable_upd_column_head(jTable_get(0), 1, 'Role');
		  } else
	if (selected == 5) { // Participants register to contribute an item or food
	    $('#tslots').hide();
	    $('#max_participants').hide();
	    $('#roles').show();
		  $('#roles legend a').html('Items to Contribute');
		  $('#role-def-frame #but-addrow').val('Add Item');
		  $('#role-def-frame label').html('Enter the items that participants can contribute:');
		  jTable_upd_column_head(jTable_get(0), 1, 'Item');
		  }
	else{
	    alert('Invalid Registration Type Selected: '+selected);
		}
}


function setup_show_elements_for_email_confirm(id) 
{
   $(id).bind("click", function () { show_elements_for_email_confirm(id);});
   show_elements_for_email_confirm(id);
}


function show_elements_for_email_confirm(id)
{

    var i;
	  var selected = -1;
	  var values = $(id);
	
	for (i=0;i<values.length;i++) {
	   if (values[i].checked) selected = i;
	}
	
	if (selected == 1) { // Always Send Confirmation Email
	    //$('#registration_confirmation').show();
	    $('#cancel_confirmation').show();
		  } else
	if (selected == 2) { // Never Send Confirmation Email
	    //$('#registration_confirmation').hide();
	    $('#cancel_confirmation').hide();
		  } else
	if (selected == 3) { // Always Send Confirmation Email
	    //$('#registration_confirmation').show();
	    $('#cancel_confirmation').show();
		  }
  else{
	    alert('Invalid Registration Type Selected: '+selected);
		  }
}