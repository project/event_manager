$id

********************************************************************
� � � � � � � � � � � D R U P A L � �M O D U L E
********************************************************************
Name: Event Manager module
Author: David Hollender, Mind Sky Internet Communications
Last update: (See CHANGELOG.txt for details)
Drupal: 5.x 
Dependencies: event module, mimemail module.  
********************************************************************
DESCRIPTION:

The Event Manager module enables web site visitors to register for events. 
Beyond basic registration, individuals can sign up to participate: 1) for a particular role or job; 
2) at a particular time; or 3) for a particular role or job at a particular time. Or, individuals 
can sign up to contribute one or more items needed for an event � for example, food for a pot-luck dinner. 

Participants do not necessarily need to be registered users of the site to sign-up for an event. 
By setting the register for events permission, you can specify which site visitors can register for 
events by role. As part of the online registration process, participants receive a confirmation code 
that may be used to change their registration, cancel it, or to send themselves a confirmation email. 

Administrators and event coordinators can view a report that lists registered users , their contact information and available slots for each event. The report can be used to track participation, determine whether there are enough volunteers, prepare for pre-event communication, or as a check-in sheet for in-person activities.
For more information, see http://drupal.org/project/directory

Want to see a demonstration? Visit:

 - http://em-demo.mind-sky.com
 
For more detailed documentation, visit:

 - http://em-demo.mind-sky.com/?q=node/3

********************************************************************
INSTALLATION:

1. Download the module and upload it to your web server. 
   For typical Drupal 5.x installations, copy the entire event_manager directory 
   into sites/all/modules.

2. Visit the modules administration page and activate Event Manager. 
   You will find it in the Event module group.

3. Setup default and system-wide settings from the Event Manager Settings Page. 
   The process is described in Event Manager's documentation.

3. From the Access Control administration page, set permissions for 
   Administer Events (Administrators), Manage Own Events (Event Coordinators) 
   and Register for Events (Participants). To allow anyone to register for an event, 
   place a checkmark in the Register for Events box for anonymous users. 
   To allow authenicated users only to register for events, place a checkmark in the 
   Register for Event for authenticated users. Be sure to click the Save Permissions 
   button when you are done.

4. Administrators and Event Coordinators will now see the "Registration Settings" tab 
   when viewing an event. Prospective participants, will see registration links when viewing an event.




