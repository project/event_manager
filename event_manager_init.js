
function define_role_table(){
    var columns = new Array();
        
    columns[columns.length] = jTable_define_column("role_group_id", "role_group_id[]", "hidden",null,null,0);
    columns[columns.length] = jTable_define_column("role_id", "role_id[]", "hidden",null,null,0);
    columns[columns.length] = jTable_define_column("role_name", "role_name[]", "text", "Role", "Enter a name for this role");
    columns[columns.length] = jTable_define_column("role_description", "role_description[]", "text", "Description", "Enter a brief description for this role");
    columns[columns.length] = jTable_define_column("role_slots", "role_nslots[]", "text", "Slots", "Enter the number of slots for this role");

    return jTable_define_table("role_table", columns);

}

function define_tslots_table(){
    var columns = new Array();
        
    columns[columns.length] = jTable_define_column("tslot-id", "tSlotId", "hidden");
    columns[columns.length] = jTable_define_column("tslot-start", "tSlotStart", "text", "Start", "Enter start time for slot");
    columns[columns.length] = jTable_define_column("tslot-end", "tSlotDuration", "text", "Duration", "Enter length of slot in minutes");

    return jTable_define_table("slots_table", columns);

}

function create_role_table(){

  handle = jTable_create_table("Roles","#role-def-frame", "role-group-1", define_role_table());
  jTable_show_table(handle);
		
  if (handle == 0) jTable_add_button("#role-def-frame", "but-addrow", "Add Role");
 //jTable_add_selector("#role-def-page-selector-frame", "table-selector");
  
  return handle;
}

$(document).ready(function() {
    var default_values =" , ,1";
		var nRows;
    jTable_init();
    event_manager_jTable_initialize();
		setup_show_elements_for_type(".registration_type");
    setup_show_elements_for_email_confirm(".email_confirm_type");
    });
