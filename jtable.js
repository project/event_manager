
/***********************************************************************************
 *  UTILITY
 ***********************************************************************************/

 function jTable_init()
 {
      jTable_debug(false);
      jTable_tables = new Array();
      jTable_default_table_options = jTable_create_table_options();
      jTable_current_view = -1;
 }
 
function jTable_debug(isDebug)
{
       if (isDebug != null) {
           jTable_is_debug = isDebug;
           if (isDebug) alert ("jTable debugging is "+ isDebug );
           }

      if (!jTable_is_debug) return false; 
 
       return jTable_is_debug;
}

function jTable_isTrue(value)
{
     if (value.substr(0,1).toUpperCase() == "Y") return true;
     if (value == "true") return true;
     if (value == 1) return true;
     return false;
}

function jTable_rowN(rowN)
{
    if (rowN) jTable_rowN_value = rowN;
	return jTable_rowN_value;
}

function jTable_get(handle)
{
     if (handle < 0)
         handle = jTable_tables.length-1;

    return jTable_tables[handle];
}


function jTable_length_from_handle(handle)
{
    return jTable_length(jTable_tables[handle].jQueryRefTable);
}

function jTable_length(objectTable)
{
       var nRows = objectTable.find("tbody > tr").get().length;
       objectTable.end();
       return nRows;
}

function jTable_find_rowN(objectTable, rowId)
{
       var rows = objectTable.find("tbody > tr").get();
       var n;

       for (n=0; n<rows.length;n++) {
             if (rows[n].id == rowId) {
                 objectTable.end();
                 return n;
                 }
           }

        objectTable.end();
        return -1;
}

function jTable_find_row(objectTable, rowId)
{
     return ($("#"+objectTable.id()+" tr#"+rowId));
}

function jTable_find_cell(objectRow, idCell)
{
      var cells= objectRow.find("td#"+idCell).get();

      if (!cells || cells.length==0) {
          if (jTable_debug()) alert("Cell "+idCell+" not found in Row "+objectRow.id());
          objectRow.end();
          return null;
          }

      var objectCell = $(cells[0]);
      if (jTable_debug()) alert("Cell "+objectCell.id()+" FOUND in Row "+objectRow.id());
      objectRow.end();
      return objectCell;
}

function jTable_find_element(objectCell, idElement)
{
      var elements= objectCell.find(idElement).get();

      if (!elements || elements.length==0) {
          if (jTable_debug()) alert("Element "+idElement+" not found in Cell "+objectCell.id());
          objectCell.end();
          return null;
          }

      var objectElement = $(elements[0]);
      if (jTable_debug()) alert("Element "+objectElement.id()+" FOUND in Cell "+objectCell.id());
      objectCell.end();
      return objectElement;
}


function jTable_set_child(objectParent, objectChild) 
{
           var arrayChildren = objectParent.attr("children");
           if (!arrayChildren) arrayChildren = new Array();

           objectChild.attr("parent", objectParent);
           arrayChildren[arrayChildren.length]= objectChild;

}

function jTable_get_child(objectParent, childName)
{
           var i;
           var arrayChildren = objectParent.attr("children");
           if (!arrayChildren) return null;

           for (i=0;i<arrayChildren.length;i++)
                 if (arrayChildren[i].attr("name") == childName)
                     return arrayChildren[i];

           return null;
}


function jTable_set_child_value(objectParent, childName, value)
{
         var child = jTable_get_child(objectParent, childName);
         if (child) {
             child.val(value);
             return true;
             }

         return false;
}

function jTable_dump_column_defs(objectTable)
{
   var n,p;
   
   for (n=0; n< objectTable.columns.length; n++) {
        document.write ("Column: "+n+"("+objectTable.columns.idName+")<br>");
        for(p=0; p<objectTable.columns[n].elements.length;p++) {
			document.write ("&nbsp;&nbsp;Element: "+p+"("+objectTable.columns[n].elements[p].qName+")<br>");
        }		
	}
}


/***********************************************************************************
 *  TABLE DEFINITION
 ***********************************************************************************/

function jTable_get_table_definition(group_id)
{
      return jTable_tables[group_id];
}


/**********************************************************************************
 * jTable_define_table
 *
 *  Combines a table name, array of column definitions and a string of options into a complete table definition.
 *  The table definition can be used to create an instance of a table. 
 *
 * Parameters:
 *
 *   name     - name of table
 *
 *   columns - array of column definitions. Column definitions include:
 *                   type - Element type (text, hidden, button)
 *                   idName - idName of css element. Used as base for values in this column for each row
 *                   qName - name of data in this column used in submitting form
 *                   headText - column header text
 *                   tipText - Tool tip text shown when cursor hovers over column header
 *                   handlerInfo - array of handler definitions. handler definitions include:
 *                                         event - name of event to be handled (e.g., "click")
 *                                         code  - reference to function that handles the event
 *
 *   options  - comma separated string of option values.  Values can be "true" or "false" for:
 *                   isSelectable - table rows can be "selected" by the user
 *                   isDeleteable - table rows can be marked for deletion by the user
 *                   isMoveUp - user can click button to move rows up
 *                   isMoveDown - user can click button to move rows down
 *
 *********************************************************************************/
function jTable_define_table(name, columns, options)
{
        var objectTableDef = new Object();
        objectTableDef.name = name;

        if    (columns)
               objectTableDef.columns = columns;
        else objectTableDef.columns = null;

        if (options) {
            var arrayOptions;
            objectTableDef.options = jTable_create_table_options();
            arrayOptions = options.split(",");
            objectTableDef.options.isSelectable = jTable_isTrue (arrayOptions[0]);
            objectTableDef.options.isDeletable  = jTable_isTrue (arrayOptions[1]);
            objectTableDef.options.isMoveUp     = jTable_isTrue (arrayOptions[2]);
            objectTableDef.options.isMoveDown = jTable_isTrue (arrayOptions[3]);
            }
      else objectTableDef.options = null;

      return objectTableDef;
}


/**********************************************************************************
 * jTable_define_element
 *
 * Create element for table. 
 * When objectParent is specified, element is a finished table cell.
 * When objectParent is NOT specified and returnDef is TRUE, element is an item (hidden field or button) to be
 *  included in a table cell. 
 *  When objectParent is NOT specified and returnDef is FALSE, html for element is returned
 *
 *  Notes
 *
 *  Element is generated as an html <input> tag.  When objectParent is spectified, element is wrapped in
 *  a <td> tag for easy inclusion in a table.  Function assumes that objectParent is a row (<tr>). When 
 *  objectParent is not specified, no <td> wrapper is generated.
 *
 *  Elements are marked with css classes to allow easy styling.  Names are generated as follows:
 *
 *  Wrapper is marked with a css id and class build from the "name" and "idSeq" that were passed:
 *       Wrapper id     =name, followed by a hyphen, then the idSeq  (e.g., myName-0)
 *                      class=name  (e.g., myName)
 *
 *  Element is marked with a css id and class build from the "name" and "idSeq" that were passed:
 *       Wrapper id     =name, then a hyphen, then the text "value-", then the idSeq  (e.g., myName-value-0)
 *                      class=name, then a hyphen, then the text "value"  (e.g., myName-value)
 *
 * Parameters:
 *   objectParent - Row into which cell will be placed.
 *   type - Element type (text, hidden, button)
 *   idSeq - Row number for uniquely identifying this instance of element from those in other rows
 *   name - Name of element
 *   value - Initial value of element. For buttons, pass the button label
 *   tipText - Tool tip text (optional)
 *   handlerName - jQuery handler name -- usually "click" (optional)
 *   handlerCode  - javascript function to invoke when event specified by handlerName occurs
 *   returnDef - When TRUE, function returns element definition that can be used to package several
 *                      elements in the same cell using jTable_add_elements.
 *
 *********************************************************************************/


function jTable_define_column(idName, qName, type, headText, tipText, defaultValue, handlerInfo)
{
        var objectColumn = new Object();

        if (!handlerInfo) handlerInfo = new Array();

        objectColumn.idName     = idName;
        objectColumn.headText   = headText;
        objectColumn.tipText    = tipText;
        objectColumn.elements   = new Array();
        objectColumn.elements[objectColumn.elements.length] = jTable_define_column_element(idName+"-value", qName, type, tipText, defaultValue, handlerInfo);

         return objectColumn;
}

function jTable_define_column_element(idName, qName, type, tipText, defaultValue, handlerInfo)
{
         var objectElement = new Object();

         objectElement.idName    = idName;
         objectElement.qName     = qName;
         objectElement.type      = type;
         objectElement.tipText   = tipText;
		 objectElement.defaultValue = defaultValue;
         objectElement.handlerInfo=handlerInfo;
         return objectElement;
}

function jTable_defineAppend_column_element (objectColumn, idName, qName, type, tipText, defaultValue, handlerInfo)
{
        objectColumn.elements[objectColumn.elements.length] = jTable_define_column_element(idName, qName, type, tipText, defaultValue, handlerInfo);
}

function jTable_define_handler(event, code)
{
        var objectHandler = new Object();

        objectHandler.event = event;
        objectHandler.code  = code;
        return objectHandler;

}

function  jTable_create_table_options()
{
       var objectOptions = new Object();

        objectOptions.isSelectable = false;
        objectOptions.isDeletable  = true;
        objectOptions.isMoveUp     = true;
        objectOptions.isMoveDown = true;
        return objectOptions;
 }

 
function jTable_define_row_header(objectTable, objectRow, rowN)
{
      var delete_control_type = "button";
      var nElement;

      var fRowUp = new Array();
      var fRowDown = new Array();
	  var fRowDelete = new Array();

      fRowUp[0] = jTable_define_handler("click", function() { jTable_row_up(objectTable.jQueryRefDiv.id(), objectRow.id()); });
      fRowDown[0] = jTable_define_handler("click", function() { jTable_row_down(objectTable.jQueryRefDiv.id(), objectRow.id()); });
      fRowDelete[0] = jTable_define_handler("click", function() { jTable_row_delete(objectTable.jQueryRefDiv.id(), objectRow.id()); });
     
 //  Create row header cell
      objectColumn = jTable_define_column("row_header", null, "hidden", null, null, rowN);
  
       nElement = 1;

     if    (!objectTable.options.isDeletable)
             objectColumn.elements[nElement++] = jTable_define_column_element("delete_row", "delete_row","hidden");
     else  objectColumn.elements[nElement++] = jTable_define_column_element("delete_row", null, "button", "delete row", "del", fRowDelete);

     if (objectTable.options.isMoveUp)
         objectColumn.elements[nElement++] = jTable_define_column_element("move_up", null, "button", "move row up", "up", fRowUp);

     if (objectTable.options.isMoveDown)
         objectColumn.elements[nElement++] = jTable_define_column_element("move_down",null,"button", "move row down", "dn", fRowDown);

      return objectColumn;

   }
 
 
/***********************************************************************************
 *  TABLE CREATION
 ***********************************************************************************/


/**********************************************************************************
 * jTable_create_table
 *
 * Add tables to be managed by jTable  
 *
 *  Notes
 *
 *  Adds tables to internal array that is used to display and manage a set of tables.  Tables are generated 
 *  using arrayTables, an array of table definitions.  Each table definition contains the following information:
 *
 *  idName - name of the table, used for creating id of table and div wrapper.  div wrapper has id of idName.
 *                 The table itself is the idName, prefixed with the text "table-"
 *  options - Table options include:  
 *                     isSelectable   - users can click to select a row
 *                     isDeletable    - users can click a checkbox to mark a row for deletion
 *                     isMoveUp      - users can click a button to move a row up
 *                     isMoveDown - users can click a button to move row down
 *
 * Parameters:
 *   idWhere - identifier of tag to hold tables.  ids should be prefixed with a #. Tags should not be prefixed
 *   arrayTables - tilde separated array of table definitions (tableId~tableName~option1`option2...)
 *   className - css class name to be used as part of table tags and div wrappers
 *
 *********************************************************************************/
function jTable_create_table(title, idWhere, idName, defTable) 
{
       var objectTable;
       var handle = jTable_tables.length;
       var html;
	   var n,p;

  //  Create object to manage table and insert in array of tables
       objectTable = new Object;
       jTable_tables[handle] = objectTable;

  //  Give table a name
       objectTable.idName = idName;
	   objectTable.title  = title;
      
 //  Initialize options
      if     (defTable.options != null) 
              objectTable.options = defTable.options;
      else objectTable.options = jTable_default_table_options;


  // Initialize column defs
      objectTable.columns = defTable.columns;

	  
  //  Create div wrapper
       html = "<div id=\""+idName+"\" class=\"jtable\"></div>";
       objectTable.jQueryRefDiv = $(html);
       
  //  Create the table and insert into wrapper
       html = "<table id=\"table-"+idName+"\"><tbody></tbody></table>";
       objectTable.jQueryRefDiv.append(html);

  //  Insert wrapper wherever caller specifies
      $(idWhere).append(objectTable.jQueryRefDiv);

 //  Get reference to jQuery Table
      objectTable.jQueryRefTable = $("#table-"+idName) ;
      if (jTable_debug() && !objectTable.jQueryRefTable) 
          alert("Cannot find table named "+idName+". Does idWhere need to be preceded by a pound sign?");

 //  Display the columns
      jTable_display_column_heads(objectTable);

	  
//////////////////////////////////////////////////////

  //  Move hidden items to row header.  This prevents attempts to display phantom columns
      for (n=1; n< objectTable.columns.length; ) {
              for(p=0; p<objectTable.columns[n].elements.length;) {
			    if (objectTable.columns[n].elements[p].type=='hidden'){
				    objectTable.columns[0].elements[objectTable.columns[0].elements.length] = objectTable.columns[n].elements[p];
					objectTable.columns[n].elements.splice(p,1);
					}
				 else {
				      p++;
					  }
			  }
			  if (objectTable.columns[n].elements.length < 1){
			      objectTable.columns.splice(n,1);
				  }
			  else n++;
        }		


////////////////////////////////////////////////////////	  
	  
	  
	  
      return handle;
 }

 function jTable_display_column_heads(objectTable)
 {

         var headerId = "table#"+objectTable.jQueryRefTable .id()+" tr#table-header-row";
         var html         = "<tr id=\"table-header-row\">";

         for (n=0;n<objectTable.columns.length;n++) {
               html += "<th id=\""+objectTable.columns[n].idName+"\" ";

			   if  (objectTable.columns[n].headText) {
			        headText = objectTable.columns[n].headText; 
                    html += " class=\"jtable-colhead\"";
					}
			   else {
  		            headText = "&nbsp;";
	                html += " class=\"jtable-colhead-empty\"";
					}

               if (objectTable.columns[n].tipText) html += " title=\""+objectTable.columns[n].tipText+"\"";
               html += ">"+headText+"</th>";
               }

             html += "</tr>";
             objectTable.jQueryRefTable .append(html);
   }

function jTable_upd_column_head(objectTable, nCol, headText)
{
    var objectHead;

    if ((nCol < 0) || (nCol > objectTable.columns.length))
	     return false;

	objectTable.columns[nCol].headText = headText;
	objectHead = $("#"+objectTable.columns[nCol].idName);
    objectHead.html(headText);
	return true;
}   

function jTable_add_row_from_string(handle, separator, values)
{
     var arrayValues = values.split(separator);
	 return jTable_add_row(handle, arrayValues);
}

function jTable_add_row(handle, arrayValues)
{
        var objectTable = jTable_get(handle);
        var id = objectTable.jQueryRefTable.id();
        var rowN = jTable_length(objectTable.jQueryRefTable);
        var n, p, q, v=0;  // iteration counters

        var idRow = "row-"+rowN;
        var rowHtml = "<tr id=\""+idRow+"\"></tr>";
        var objectRow;
        var objectCell;

        objectTable.jQueryRefTable.append(rowHtml);
		objectHeader = jTable_find_row(objectTable.jQueryRefTable, "row-0");
        objectRow = jTable_find_row(objectTable.jQueryRefTable, idRow);
        defColumnRowHeader = jTable_define_row_header(objectTable, objectRow, rowN);


        for (n=0; n<= objectTable.columns.length; n++) {
              if (n==0)
                      defColumn = defColumnRowHeader;
              else defColumn = objectTable.columns[n-1];

         //  Build cell that wraps element(s)
              html = "<td id=\""+defColumn.idName+"-"+rowN+"\" class=\""+defColumn.idName+"\"></td>";
              objectRow.append(html);
			  objectCell = jTable_find_cell(objectRow, defColumn.idName+"-"+rowN);

         //  Fill cell with element(s)
              for(p=0; p<defColumn.elements.length; p++) {
               //  Create element
                    if (n==0) { if (defColumn.elements[p].defaultValue) value = defColumn.elements[p].defaultValue; else value = ""; } else
					if (arrayValues && arrayValues[v]) value = arrayValues[v++]; else
					if ((defColumn.elements[p].type == "button") && (defColumn.elements[p].tipText)) value = defColumn.elements[p].tipText;
					else value = "";
                    html = jTable_build_element(defColumn.elements[p], defColumn.idName, rowN, p, value);
                    objectCell.append(html);
                    objectElement = jTable_find_element(objectCell, "#"+defColumn.idName+"-"+rowN+"-value-"+p);
				
               //  Bind event handlers, if any
			       if (defColumn.elements[p].handlerInfo)
                   for (q=0; q<defColumn.elements[p].handlerInfo.length; q++){
                         objectElement.bind(defColumn.elements[p].handlerInfo[q].event, defColumn.elements[p].handlerInfo[q].code);
						 }
                   }

          }
		  

}

function jTable_build_element(defElement, idName, rowN, idSeq, value)
{

     var html;
     var idNameElement      = idName+"-"+rowN+"-value-"+idSeq;
     var classNameElement = idName+"-value";
	 var qName = "";
	 var titleAttr = "";

     if (defElement.tipText) titleAttr = " title=\""+defElement.tipText+"\"";
     if (!value) value = "";
   	 if (defElement.qName) qName = " name=\""+defElement.qName+"\"";
	 html = "<input id=\""+idNameElement+"\" class=\""+classNameElement+"\""+qName+" value=\""+value+"\" type=\""+defElement.type+"\" "+ titleAttr+">";	
	 return html;
}

/***********************************************************************************
 *  WIDGETS
 ***********************************************************************************/
   
function jTable_add_selector(idWhere, idName)
{
       var i;
       var values;
       var html = "<select id=\""+idName+"\">";
       for (i=0;i<jTable_tables.length;i++){
             html+="<option value=\""+i+"\"";
             if (i == 0) html+=" selected";
             html += ">"+jTable_tables[i].title+"</option>";
             }
       html += "</select>";
       $(idWhere).append(html);
 $("#"+idName).bind("change", function(){
      jTable_current_view= $("#"+idName).val();
      jTable_show_table($("#"+idName).val());
});

}

function jTable_add_button(idWhere, idName, text)
{
    var html = "<input type=\"button\" value=\""+text+"\" id=\""+idName+"\">";
	
    $(idWhere).append(html);
 
    $("#"+idName).bind("click", 
	                   function(){
                        jTable_add_row(jTable_current_view);
                        });
}
   

/***********************************************************************************
 *  OPERATIONS
 ***********************************************************************************/

function jTable_hide_all()
{
     var objectTable;
     var n;

      for (n=0; n<jTable_tables.length;n++)
             $("#"+jTable_tables[n].idName).hide();
  
}
function jTable_show_table(n, duration)
{
     var objectTable;
     var n;

     if (n >= jTable_tables.length) {
         alert("selected table cannot be displayed");
         return false;
         }
     objectTable = $("#"+jTable_tables[n].idName);

     jTable_hide_all();
     objectTable.show(duration);
     jTable_current_view = n;
}


function jTable_row_up(idParent, idRow)
{
      var parent = $("#"+idParent);
      var rowN = jTable_find_rowN(parent, idRow);

       if (rowN <= 1) return;
       var src = $("#"+idRow);
       var dest = $("#"+idRow).prev();
       src.insertBefore(dest);  
}

function jTable_row_down(idParent, idRow)
{
      var parent = $("#"+idParent);
      var rowN = jTable_find_rowN(parent, idRow);

       if (rowN >= jTable_length(parent)) return;
       var src = $("#"+idRow);
       var dest = $("#"+idRow).next();
       src.insertAfter(dest);  
}


function jTable_row_delete(idParent, idRow)
{
      var parent = $("#"+idParent);
	  var row = jTable_find_row(parent, idRow);
	  row.remove();
}


